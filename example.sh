#!/bin/sh

case "$1" in
    16)
	C_X=0x0000
	C_W=0xffff
	C_R=0xf800
	C_G=0x07e0
	C_B=0x001f
	;;
    24)
	C_X=0x000000
	C_W=0xffffff
	C_R=0xff0000
	C_G=0x00ff00
	C_B=0x0000ff
	;;
    *)
    	echo "Usage: $0 <depth>" >&2
	exit 1
	;;
esac

./xwindow_check <<EOF
print *** Creating top level
create 1:0:640x480+0+0:$C_X
expose 1:640x480+0+0
checkexposes 1
checkpixels 1:640x480+0+0:$C_X

print *** Creating 1st child
create 2:1:320x240+0+0:$C_R
expose 2:320x240+0+0
checkexposes 1
checkexposes 2
checkpixels 1:320x240+0+0:$C_R
checkpixels 1:320x240+320+0:$C_X
checkpixels 1:640x240+0+240:$C_X

print *** Creating 2nd child
create 3:1:320x240+160+120:$C_G
expose 3:320x240+0+0
checkexposes 1
checkexposes 2
checkexposes 3

print *** Creating 3rd child
create 4:1:320x240+320+240:$C_B
expose 4:320x240+0+0
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4

print *** Auto redirecting 2nd child
redirect 3:automatic
expose 3:160x120+160+120
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4
checkpixels 1:320x120+0+0:$C_R
checkpixels 1:160x120+0+120:$C_R
checkpixels 1:160x120+160+120:$C_G
checkpixels 3:320x240+0+0:$C_G

print *** Unredirecting 2nd child
unredirect 3:automatic
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4

print *** Moving 2nd child
moveresize 3:320x240+160+0
expose 3:160x120+160+120
expose 1:160x120+160+240
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4
checkpixels 1:160x240+0+0:$C_R
checkpixels 1:160x240+160+0:$C_G
checkpixels 3:320x240+0+0:$C_G

print *** Moving 2nd child back
moveresize 3:320x240+160+120
expose 2:160x120+160+0
expose 1:160x120+320+0
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4

print *** Manual redirecting 2nd child
redirect 3:manual
expose 3:160x120+160+120
expose 2:160x120+160+120
expose 1:160x120+320+120
expose 1:160x120+160+240
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4
checkpixels 1:320x240+0+0:$C_R
checkpixels 1:320x240+320+0:$C_X
checkpixels 1:320x240+0+240:$C_X
checkpixels 1:320x240+320+240:$C_B

print *** Moving 2nd child
moveresize 3:320x240+0+120
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4

print *** Moving 2nd child back
moveresize 3:320x240+160+120
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4

print *** Unredirecting 2nd child
unredirect 3:manual
checkexposes 1
checkexposes 2
checkexposes 3
checkexposes 4

print *** Done
sleep 1
EOF
