/*
 * Copyright (c) 2010  Nokia Corporation
 * Copyright (c) 2011  Ville Syrjala <syrjala@sci.fi>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "region.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xcomposite.h>

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))

static Display *dpy;
static int screen;
static Window root;
static int retval;
static bool check_undefpixels;
static bool override_undefpixels;
static unsigned int color_undefpixels;

enum {
	ERROR_OPEN_FAILED         = 0x1,
	ERROR_READ_FAILED         = 0x2,
	ERROR_BAD_INPUT           = 0x4,
	ERROR_EXPOSE_NOT_EXPECTED = 0x8,
	ERROR_EXPOSE_NOT_RECEIVED = 0x10,
	ERROR_PIXEL_MISMATCH      = 0x20,
	ERROR_CMDLINE             = 0x40,
};

enum {
	COLOR_BLACK,
	COLOR_WHITE,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_BLUE,
};

static const char *color_names[] = {
	[COLOR_BLACK] = "black",
	[COLOR_WHITE] = "white",
	[COLOR_RED  ] = "red",
	[COLOR_GREEN] = "green",
	[COLOR_BLUE ] = "blue",
};

static const unsigned short colors565[] = {
	[COLOR_BLACK] = 0x0000,
	[COLOR_WHITE] = 0xffff,
	[COLOR_RED  ] = 0xf800,
	[COLOR_GREEN] = 0x07e0,
	[COLOR_BLUE ] = 0x001f,
};

static const unsigned int colors888[] = {
	[COLOR_BLACK] = 0x000000,
	[COLOR_WHITE] = 0xffffff,
	[COLOR_RED  ] = 0xff0000,
	[COLOR_GREEN] = 0x00ff00,
	[COLOR_BLUE ] = 0x0000ff,
};

struct pixels
{
	int x;
	int y;
	unsigned int width;
	unsigned int height;
	unsigned int color;
};

struct window {
	unsigned int id;
	unsigned int parent_id;

	Window xid;
	Window parent_xid;

	int x;
	int y;

	unsigned int width;
	unsigned int height;

	unsigned int depth;

	unsigned int color;

	struct {
		RegionRec expected;
		RegionRec received;
	} expose;

	XImage *image;

	unsigned int pixels_size;
	unsigned int pixels_valid;
	struct pixels *pixels;

	bool no_background;

	bool mapped;
};

static struct window *windows;
static unsigned int windows_size;
static unsigned int windows_valid;

static void help_args(const char *name)
{
	printf("Usage: %s [-h|--help] [-u|--undefpixels [color]] [file]\n"
	       "Input will be read from 'file' or standard input\n"
	       "\n", name);
}

static void help_commands(void)
{
	printf("Supported commands:\n"
	       " create <id>:<parent>:<width>x<height>+<x>+<y>:<depth>:<color>\n"
	       "  Create a new window, color is either hex, color name, or 'none'\n"
	       " destroy <id>\n"
	       "  Destroy a window\n"
	       " redirect <id>:<redirect>\n"
	       "  Redirect a window\n"
	       " unredirect <id>:<redirect>\n"
	       "  Unredirect a window\n"
	       " reparent <id>:<parent>\n"
	       "  Reparent a window\n"
	       " moveresize <id>:<width>x<height>+<x>+<y>\n"
	       "  Move and/or resize a window\n"
	       " map <id>\n"
	       "  Map a window window\n"
	       " unmap <id>\n"
	       "  Unmap a window window\n"
	       " expose <id>:<width>x<height>+<x>+<y>\n"
	       "  Expect expose events to cover the specified area\n"
	       " checkexposes [id]\n"
	       "  Check that the expose events for the window cover the expected area\n"
	       "  Checks all windows if id isn't specified\n"
	       " pixels <id>:<width>x<height>+<x>+<y>:<color>\n"
	       "  Expect the pixel contents in the area of the window to match the specific color\n"
	       " undefpixels <id>:<width>x<height>+<x>+<y>:<color>\n"
	       "  Expect the pixel contents in the area of the window to match the specific color\n"
	       "  Checking must be enabled with the -u|--undefpixels option.\n"
	       "  If you expect all undefined pixels to have a specific color you can specify it.\n"
	       " checkpixels [id]\n"
	       "  Check that the pixels in the window match the expected colors\n"
	       "  Checks all windows if id isn't specified\n"
	       " copyarea <sid>:<did>:+<sx>+<sy>:<width>x<height>:+<dx>+<dy>\n"
	       "  Copies content between windows\n"
	       " sleep <seconds>\n"
	       "  Sleep for some seconds\n"
	       " usleep <microseconds>\n"
	       "  Sleep for some microseconds\n"
	       " print <message>\n"
	       "  Print the message\n");
}

static struct window *
add_window(const struct window *w)
{
	struct window *new_windows;

	if (windows_valid == windows_size) {
		windows_size += 8;
		new_windows = realloc(windows, windows_size * sizeof(struct window));
		if (!new_windows)
			return NULL;
		windows = new_windows;
	}

	windows[windows_valid++] = *w;

	return &windows[windows_valid - 1];
}

static bool
rm_window(const struct window *w)
{
	unsigned int i;

	for (i = 0; i < windows_valid; i++) {
		if (windows[i].id != w->id)
			continue;
		windows_valid--;
		if (i < windows_valid)
			memmove(&windows[i], &windows[i+1],
				(windows_valid - i) * sizeof(struct window));
		return true;
	}

	return false;
}

static bool
for_all_windows(bool (*func)(struct window *w))
{
	unsigned int i;

	for (i = 0; i < windows_valid; i++)
		if (!func(&windows[i]))
			return false;

	return true;
}

struct window *
lookup_window(unsigned int id)
{
	unsigned int i;

	for (i = 0; i < windows_valid; i++) {
		if (windows[i].id != id)
			continue;
		return &windows[i];
	}
	return NULL;
}

struct window *
lookup_xwindow(Window win)
{
	unsigned int i;

	for (i = 0; i < windows_valid; i++) {
		if (windows[i].xid != win)
			continue;
		return &windows[i];
	}
	return NULL;
}

static void received_expose(struct window *w, int x, int y,
			    unsigned width, unsigned height)
{
	BoxRec box = {
		.x1 = x,
		.y1 = y,
		.x2 = x + width,
		.y2 = y + height,
	};
	RegionRec region;

	RegionInit(&region, &box, 0);
	RegionAppend(&w->expose.received, &region);
}

static void expected_expose(struct window *w, int x, int y,
			    unsigned width, unsigned height)
{
	BoxRec box = {
		.x1 = x,
		.y1 = y,
		.x2 = x + width,
		.y2 = y + height,
	};
	RegionRec region;

	RegionInit(&region, &box, 0);
	RegionAppend(&w->expose.expected, &region);
}

static bool process_events(struct window *target, bool target_mapped)
{
	XSync(dpy, False);

	while (XPending(dpy) > 0 ||
	       (target != NULL && target->mapped != target_mapped)) {
		XEvent e;

		if (XNextEvent(dpy, &e) != Success)
			return false;

		switch (e.type) {
			struct window *w;
		case Expose:
			w = lookup_xwindow(e.xexpose.window);
			if (!w)
				return false;

			received_expose(w,
					e.xexpose.x,
					e.xexpose.y,
					e.xexpose.width,
					e.xexpose.height);
			break;
		case CirculateNotify:
		case ConfigureNotify:
		case DestroyNotify:
		case GravityNotify:
		case ReparentNotify:
			break;
		case MapNotify:
			w = lookup_xwindow(e.xmap.window);
			if (!w)
				return false;

			w->mapped = true;
			break;
		case UnmapNotify:
			w = lookup_xwindow(e.xunmap.window);
			if (!w)
				return false;

			w->mapped = false;
			break;
		default:
			fprintf(stderr, "Unknown event %u\n", e.type);
			return false;
		}
	}

	return true;
}

static void set_wm_props(Window win, struct window *w)
{
	char name[64];
	XSizeHints sizeHints;
	XWMHints wmHints;
	XTextProperty wm_name, icon_name;

	snprintf(name, sizeof name, "xwindow_check %u", w->id);

	sizeHints.flags = 0;

	wmHints.flags = InputHint;
	wmHints.input = True;

	wm_name.value = (unsigned char *) name;
	wm_name.encoding = XA_STRING;
	wm_name.format = 8;
	wm_name.nitems = strlen (name) + 1;

	icon_name = wm_name;

	XSetWMProperties(dpy, win,
			 &wm_name, &icon_name,
			 NULL, 0,
			 &sizeHints, &wmHints, 0);
}

static bool
window_depth(unsigned int id, unsigned int *ret_depth)
{
	struct window *w;

	w = lookup_window(id);
	if (!w)
		return false;

	*ret_depth = w->depth;
	return true;
}

static bool
create_window(struct window *w)
{
	Window win;
	struct window *parent;
	XSetWindowAttributes wattr;
	unsigned long mask = CWEventMask | CWOverrideRedirect;
	Visual *visual;

	if (lookup_window(w->id))
		return false;

	parent = lookup_window(w->parent_id);
	if (!parent)
		return false;

	wattr.event_mask = ExposureMask | StructureNotifyMask;
	wattr.override_redirect = parent->xid == root;

	if (w->no_background) {
		mask |= CWBackPixmap;
		wattr.background_pixmap = None;
	} else {
		mask |= CWBackPixel;
		wattr.background_pixel = w->color;
	}

	if (w->depth != parent->depth) {
		static XVisualInfo vi = { .visual = NULL, };

		if (!XMatchVisualInfo(dpy, screen, w->depth, TrueColor, &vi))
			return false;

		mask |= CWColormap;
		wattr.colormap = XCreateColormap(dpy, parent->xid, vi.visual, AllocNone);

		mask |= CWBorderPixel;
		wattr.border_pixel = 0;

		w->depth = vi.depth;
		visual = vi.visual;
	} else {
		w->depth = parent->depth;
		visual = CopyFromParent;
	}

	win = XCreateWindow(dpy, parent->xid, w->x, w->y, w->width, w->height, 0,
			    w->depth, InputOutput, visual, mask, &wattr);

	set_wm_props(win, w);

	XMapWindow(dpy, win);

	w->xid = win;
	w->parent_xid = parent->xid;

	RegionNull(&w->expose.expected);
	RegionNull(&w->expose.received);

	w = add_window(w);
	if (!w)
		return false;

	if (!process_events(w, true))
		return false;

	return true;
}

static bool
destroy_window(struct window *w)
{
	XDestroyWindow(dpy, w->xid);

	XSync(dpy, False);

	return rm_window(w);
}

static bool init_x(void)
{
	struct window w = { .id = 0, };
	unsigned int border;
	unsigned int depth;

	dpy = XOpenDisplay(NULL);
	if (!dpy)
		return false;

	screen = DefaultScreen (dpy);
	root = RootWindow (dpy, screen);

	XGetGeometry(dpy, root, &root, &w.x, &w.y, &w.width, &w.height, &border, &depth);

	w.id = 0;
	w.parent_id = 0;
	w.xid = root;
	w.parent_xid = None;
	w.depth = depth;
	w.mapped = true;

	RegionNull(&w.expose.expected);
	RegionNull(&w.expose.received);

	return add_window(&w) != NULL;
}

static bool deinit_x(void)
{
	XCloseDisplay(dpy);
	return true;
}

static bool
parse_color(const char *s,
	    unsigned int depth,
	    unsigned int *ret_color)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(color_names); i++) {
		if (color_names[i] &&
		    !strcmp(s, color_names[i])) {
			switch (depth) {
			case 16:
				*ret_color = colors565[i];
				return true;
			case 24:
			case 32:
				*ret_color = colors888[i];
				return true;
			default:
				return false;
			}
		}
	}

	return sscanf(s, "0x%x", ret_color) == 1;
}

static bool
parse_create(const char *s)
{
	struct window w = { .id = 0, };
	char tmp[16];

	if (sscanf(s, "%u:%u:%ux%u+%d+%d:%u:%15s", &w.id, &w.parent_id,
		   &w.width, &w.height, &w.x, &w.y, &w.depth, tmp) != 8)
		return false;

	if (!w.depth && !window_depth(w.parent_id, &w.depth))
		return false;

	if (!strcmp(tmp, "none"))
		w.no_background = true;
	else if (!parse_color(tmp, w.depth, &w.color))
		return false;

	return create_window(&w);
}

static bool
parse_destroy(const char *s)
{
	unsigned int id;
	struct window *w;

	if (sscanf(s, "%u", &id) != 1)
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	return destroy_window(w);
}

static bool reparent_window(struct window *w,
			    struct window *parent,
			    int x, int y)
{
	XReparentWindow(dpy, w->xid, parent->xid, x, y);

	w->x = x;
	w->y = y;
	w->parent_id = parent->id;
	w->parent_xid = parent->xid;

	return true;
}

static bool
parse_reparent(const char *s)
{
	unsigned int id;
	unsigned int parent_id;
	struct window *w, *parent;
	int x, y;

	if (sscanf(s, "%u:%u:+%d+%d", &id, &parent_id, &x, &y) != 4)
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	parent = lookup_window(parent_id);
	if (!parent)
		return false;

	return reparent_window(w, parent, x, y);
}

static bool
redirect_window(struct window *w, int redirect)
{
	XCompositeRedirectWindow(dpy, w->xid, redirect);

	return true;
}

static bool
parse_redirect(const char *s)
{
	unsigned int id;
	int redirect;
	char redirect_str[10];
	struct window *w;

	if (sscanf(s, "%u:%9s", &id, redirect_str) != 2)
		return false;
	redirect_str[sizeof redirect_str - 1] = 0;

	if (!strcmp(redirect_str, "automatic"))
		redirect = CompositeRedirectAutomatic;
	else if (!strcmp(redirect_str, "manual"))
		redirect = CompositeRedirectManual;
	else
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	return redirect_window(w, redirect);
}

static bool
unredirect_window(struct window *w, int redirect)
{
	XCompositeUnredirectWindow(dpy, w->xid, redirect);

	return true;
}

static bool
parse_unredirect(const char *s)
{
	unsigned int id;
	int redirect;
	char redirect_str[10];
	struct window *w;

	if (sscanf(s, "%u:%9s", &id, redirect_str) != 2)
		return false;
	redirect_str[sizeof redirect_str - 1] = 0;

	if (!strcmp(redirect_str, "automatic"))
		redirect = CompositeRedirectAutomatic;
	else if (!strcmp(redirect_str, "manual"))
		redirect = CompositeRedirectManual;
	else
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	return unredirect_window(w, redirect);
}

static bool
parse_sleep(const char *s)
{
	unsigned int time;

	if (sscanf(s, "%u", &time) != 1)
		return false;

	do {
		time = sleep(time);
	} while (time);

	return true;
}

static bool
parse_usleep(const char *s)
{
	unsigned int time;

	if (sscanf(s, "%u", &time) != 1)
		return false;

	while (usleep(time) != 0)
		;

	return true;
}

static bool
parse_print(const char *s)
{
	printf("%s", s);

	return true;
}

static bool
moveresize_window(struct window *w,
		  int x,
		  int y,
		  unsigned int width,
		  unsigned int height)
{
	XMoveResizeWindow(dpy, w->xid, x, y, width, height);

	w->x = x;
	w->y = y;
	w->width = width;
	w->height = height;

	return true;
}

static bool
map_window(struct window *w)
{
	XMapWindow(dpy, w->xid);

	if (!process_events(w, true))
		return false;

	return true;
}

static bool
parse_map(const char *s)
{
	unsigned int id;
	struct window *w;

	if (sscanf(s, "%u", &id) != 1)
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	return map_window(w);
}

static bool
unmap_window(struct window *w)
{
	XUnmapWindow(dpy, w->xid);

	if (!process_events(w, false))
		return false;

	return true;
}

static bool
parse_unmap(const char *s)
{
	unsigned int id;
	struct window *w;

	if (sscanf(s, "%u", &id) != 1)
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	return unmap_window(w);
}

static bool
parse_moveresize(const char *s)
{
	unsigned int id;
	unsigned int width, height;
	int x, y;
	struct window *w;

	if (sscanf(s, "%u:%ux%u+%d+%d", &id, &width, &height, &x, &y) != 5)
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	return moveresize_window(w, x, y, width, height);
}

static bool check_exposes_not_received(struct window *w)
{
	RegionRec not_received;
	Bool overlap;

	/* exposes that were expected but not received */
	RegionNull(&not_received);
	RegionSubtract(&not_received, &w->expose.expected, &w->expose.received);
	RegionValidate(&not_received, &overlap);

	if (RegionNotEmpty(&not_received)) {
		printf("Window %u non exposed areas that were expected:\n", w->id);
		RegionPrint(&not_received);
		return false;
	}

	return true;
}

static bool check_exposes_not_expected(struct window *w)
{
	RegionRec not_expected;
	Bool overlap;

	/* exposes that were not expected but were received */
	RegionNull(&not_expected);
	RegionSubtract(&not_expected, &w->expose.received, &w->expose.expected);
	RegionValidate(&not_expected, &overlap);

	if (RegionNotEmpty(&not_expected)) {
		printf("Window %u exposed areas that were not expected:\n", w->id);
		RegionPrint(&not_expected);
		return false;
	}

	return true;
}

static bool
parse_expose(const char *s)
{
	unsigned int id;
	unsigned int width, height;
	int x, y;
	struct window *w;

	if (sscanf(s, "%u:%ux%u+%d+%d", &id, &width, &height, &x, &y) != 5)
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	expected_expose(w, x, y, width, height);

	return true;
}

static bool checkexposes(struct window *w)
{
	Bool overlap;

	RegionValidate(&w->expose.expected, &overlap);
	RegionValidate(&w->expose.received, &overlap);

	if (!check_exposes_not_expected(w))
		retval |= ERROR_EXPOSE_NOT_EXPECTED;
	if (!check_exposes_not_received(w))
		retval |= ERROR_EXPOSE_NOT_RECEIVED;

	RegionEmpty(&w->expose.expected);
	RegionEmpty(&w->expose.received);

	return true;
}

static bool
parse_checkexposes(const char *s)
{
	unsigned int id;
	struct window *w;

	if (*s == '\0') {
		if (!process_events(NULL, false))
			return false;

		return for_all_windows(checkexposes);
	} else {
		if (sscanf(s, "%u", &id) != 1)
			return false;

		w = lookup_window(id);
		if (!w)
			return false;

		if (!process_events(NULL, false))
			return false;

		return checkexposes(w);
	}
}

static bool
getimage(struct window *w)
{
	XImage *image;

	if (w->image)
		return false;

	image = XGetImage(dpy, w->xid, 0, 0, w->width, w->height, AllPlanes, ZPixmap);
	if (!image)
		return false;

	assert(image->width == w->width);
	assert(image->height == w->height);
	assert(image->xoffset >= 0);
	assert(image->format == ZPixmap);
	assert(image->data != NULL);
	assert(image->depth > 0);
	assert(image->bytes_per_line > 0);
	assert(image->bits_per_pixel > 0);
	assert(image->red_mask != 0);
	assert(image->blue_mask != 0);
	assert(image->green_mask != 0);

	switch (image->bits_per_pixel) {
	case 8:
	case 16:
	case 32:
		break;
	default:
		XDestroyImage(image);
		return false;
	}

	w->image = image;

	return true;
}

static bool
destroyimage(struct window *w)
{
	if (!w->image)
		return false;

	XDestroyImage(w->image);
	w->image = NULL;

	return true;
}

static bool
add_pixels(struct window *w,
	   int x,
	   int y,
	   unsigned int width,
	   unsigned int height,
	   unsigned int color)
{
	struct pixels *new_pixels;
	struct pixels p = {
		.x = x,
		.y = y,
		.width = width,
		.height = height,
		.color = color,
	};

	if (w->pixels_valid == w->pixels_size) {
		w->pixels_size += 8;
		new_pixels = realloc(w->pixels, w->pixels_size * sizeof(struct pixels));
		if (!new_pixels)
			return false;
		w->pixels = new_pixels;
	}

	w->pixels[w->pixels_valid++] = p;

	return true;
}

static bool
parse_pixels(const char *s,
	     bool override, unsigned int override_color)
{
	unsigned int id;
	unsigned int width, height;
	int x, y;
	unsigned int color;
	struct window *w;
	char tmp[16];

	if (sscanf(s, "%u:%ux%u+%d+%d:%15s", &id, &width, &height, &x, &y, tmp) != 6)
		return false;

	w = lookup_window(id);
	if (!w)
		return false;

	if (!parse_color(tmp, w->depth, &color))
		return false;

	if (override)
		color = override_color;

	add_pixels(w, x, y, width, height, color);

	return true;
}

static bool
parse_undefpixels(const char *s)
{
	if (!check_undefpixels)
		return true;

	return parse_pixels(s, override_undefpixels, color_undefpixels);
}

static void
clear_pixels(struct window *w)
{
	w->pixels_valid = 0;
	w->pixels_size = 0;
	free(w->pixels);
	w->pixels = NULL;
}

static bool
do_checkpixels(struct window *w, int x, int y,
	       unsigned int width, unsigned int height,
	       unsigned int color)
{
	XImage *image = w->image;
	int ix, iy;

	if (!image)
		return false;
	if (width > w->width || x > w->width - width)
		return false;
	if (height > w->height || y > w->height - height)
		return false;

	if (color & ~(image->red_mask | image->green_mask | image->blue_mask))
		return false;

	switch (image->bits_per_pixel) {
	case 8:
		for (iy = 0; iy < height; iy++) {
			uint8_t *ptr = (uint8_t *) image->data
				+ (y + iy) * image->bytes_per_line
				+ (image->xoffset + x);
			for (ix = 0; ix < width; ix++) {
				if (ptr[ix] != color) {
					printf("Window %u pixel mismatch at +%d+%d: "
					       "read 0x%02x, expected 0x%02x\n",
					       w->id, x + ix, y + iy, ptr[ix], color);
					retval |= ERROR_PIXEL_MISMATCH;
				}
			}
		}
		break;
	case 16:
		for (iy = 0; iy < height; iy++) {
			uint16_t *ptr = (uint16_t*)((uint8_t*) image->data
						    + (y + iy) * image->bytes_per_line
						    + (image->xoffset + x) * 2);
			for (ix = 0; ix < width; ix++) {
				if (ptr[ix] != color) {
					printf("Window %u pixel mismatch at +%d+%d: "
					       "read 0x%04x, expected 0x%04x\n",
					       w->id, x + ix, y + iy, ptr[ix], color);
					retval |= ERROR_PIXEL_MISMATCH;
				}
			}
		}
		break;
	case 32:
		for (iy = 0; iy < height; iy++) {
			uint32_t *ptr = (uint32_t*)((uint8_t *)image->data
						    + (y + iy) * image->bytes_per_line
						    + (image->xoffset + x) * 4);
			for (ix = 0; ix < width; ix++) {
				if (ptr[ix] != color) {
					printf("Window %u pixel mismatch at +%d+%d: "
					       "read 0x%08x, expected 0x%08x\n",
					       w->id, x + ix, y + iy, ptr[ix], color);
					retval |= ERROR_PIXEL_MISMATCH;
				}
			}
		}
 		break;
	default:
		XDestroyImage(image);
		return false;
	}

	return true;
}

static bool
checkpixels(struct window *w)
{
	unsigned int i;

	if (w->pixels)
		getimage(w);

	for (i = 0; i < w->pixels_valid; i++) {
		if (!do_checkpixels(w,
				    w->pixels[i].x,
				    w->pixels[i].y,
				    w->pixels[i].width,
				    w->pixels[i].height,
				    w->pixels[i].color))
			return false;
	}

	if (w->image)
		destroyimage(w);

	clear_pixels(w);

	return true;
}

static bool
parse_checkpixels(const char *s)
{
	unsigned int id;
	struct window *w;

	if (*s == '\0') {
		return for_all_windows(checkpixels);
	} else {
		if (sscanf(s, "%u", &id) != 1)
			return false;

		w = lookup_window(id);
		if (!w)
			return false;

		return checkpixels(w);
	}
}

static bool
copyarea(struct window *sw, struct window *dw,
	 int sx, int sy,
	 unsigned int width, unsigned int height,
	 int dx, int dy)
{
	XGCValues values = {
		.subwindow_mode = IncludeInferiors,
		.graphics_exposures = False,
	};
	GC gc = XCreateGC(dpy, dw->xid, GCSubwindowMode | GCGraphicsExposures, &values);

	XCopyArea(dpy, sw->xid, dw->xid, gc,
		  sx, sy, width, height, dx, dy);

	XFreeGC(dpy, gc);

	return true;
}

static bool
parse_copyarea(const char *s)
{
	unsigned int sid, did;
	unsigned int width, height;
	int sx, sy, dx, dy;
	struct window *sw, *dw;

	if (sscanf(s, "%u:%u:+%d+%d:%ux%u:+%d+%d",
		   &sid, &did, &sx, &sy, &width, &height, &dx, &dy) != 8)
		return false;

	sw = lookup_window(sid);
	if (!sw)
		return false;

	dw = lookup_window(did);
	if (!dw)
		return false;

	return copyarea(sw, dw, sx, sy, width, height, dx, dy);
}

static bool
parse_action(const char *s, const char *v)
{
	if (!strcmp(s, "create")) {
		return parse_create(v);
	} else if (!strcmp(s, "destroy")) {
		return parse_destroy(v);
	} else if (!strcmp(s, "redirect")) {
		return parse_redirect(v);
	} else if (!strcmp(s, "unredirect")) {
		return parse_unredirect(v);
	} else if (!strcmp(s, "reparent")) {
		return parse_reparent(v);
	} else if (!strcmp(s, "moveresize")) {
		return parse_moveresize(v);
	} else if (!strcmp(s, "map")) {
		return parse_map(v);
	} else if (!strcmp(s, "unmap")) {
		return parse_unmap(v);
	} else if (!strcmp(s, "expose")) {
		return parse_expose(v);
	} else if (!strcmp(s, "checkexposes")) {
		return parse_checkexposes(v);
	} else if (!strcmp(s, "pixels")) {
		return parse_pixels(v, false, 0);
	} else if (!strcmp(s, "undefpixels")) {
		return parse_undefpixels(v);
	} else if (!strcmp(s, "checkpixels")) {
		return parse_checkpixels(v);
	} else if (!strcmp(s, "copyarea")) {
		return parse_copyarea(v);
	} else if (!strcmp(s, "sleep")) {
		return parse_sleep(v);
	} else if (!strcmp(s, "usleep")) {
		return parse_usleep(v);
	} else if (!strcmp(s, "print")) {
		return parse_print(v);
	} else {
		return false;
	}
}

int main(int argc, char *argv[])
{
	FILE *f = stdin;
	const char *fname = NULL;
	bool help = false;
	int i;

	for (i = 1; i < argc; ) {
		if (!strcmp(argv[i], "-h") ||
		    !strcmp(argv[i], "--help")) {
			help = true;
			i++;
		} else if (!strcmp(argv[i], "-u") ||
			   !strcmp(argv[i], "--undefpixels")) {
			check_undefpixels = true;
			i++;

			if (i < argc &&
			    sscanf(argv[i], "0x%x", &color_undefpixels) == 1) {
				override_undefpixels = true;
				i++;
			}
		} else if (i == argc - 1) {
			if (argv[i][0] == '-' &&
			    argv[i][1] != '\0') {
				retval |= ERROR_CMDLINE;
				break;
			}

			if (argv[i][0] != '-')
				fname = argv[i];

			i++;
		} else {
			retval |= ERROR_CMDLINE;
			break;
		}
	}

	if (help || retval) {
		help_args(argv[0]);
		return retval;
	}

	if (fname)
		f = fopen(fname, "r");
	if (!f) {
		fprintf(stderr, "Failed to open for reading '%s'\n", fname);
		help_args(argv[0]);
		return ERROR_OPEN_FAILED;
	}

	init_x();

	if (f == stdin && isatty(fileno(stdin))) {
		char *buf;
		char *s;
		char *cmd, *params;

		printf("Interactive mode\n");

		for (;;) {
			buf = s = readline("> ");
			if (!s)
				break;
			while (isspace(*s))
				s++;
			if (*s == '\0' || *s == '#')
				continue;
			add_history(s);
			cmd = s;
			s = strchr(s, ' ');
			if (!s) {
				s = cmd;
				while (isalpha(*s))
					s++;
			}
			if (isspace(*s))
				*s++ = '\0';
			while (isspace(*s))
				s++;
			params = s;
			if (!parse_action(cmd, params)) {
				fprintf(stderr, "Invalid input: '%s %s'\n", cmd, params);
				help_commands();
				retval |= ERROR_BAD_INPUT;
				continue;
			}

			free(buf);
		}
	} else {
 		char buf[256];
		char *s;
		char *cmd, *params;

		printf("Batch mode, reading from '%s'\n",
		       f == stdin ? "standard input" : fname);

		for (;;) {
			s = fgets(buf, sizeof buf, f);
			if (!s) {
				if (feof(f))
					break;
				fprintf(stderr, "Error reading input\n");
				help_args(argv[0]);
				return ERROR_READ_FAILED;
			}
			while (isspace(*s))
				s++;
			if (*s == '\0' || *s == '#')
				continue;
			cmd = s;
			s = strchr(s, ' ');
			if (!s) {
				s = cmd;
				while (isalpha(*s))
					s++;
			}
			if (isspace(*s))
				*s++ = '\0';
			while (isspace(*s))
				s++;
			params = s;
			if (!parse_action(cmd, params)) {
				fprintf(stderr, "Invalid input: '%s %s'\n", cmd, params);
				help_commands();
				retval |= ERROR_BAD_INPUT;
				break;
			}
		}
	}

	deinit_x();

	if (f != stdin)
		fclose(f);

	return retval;
}
