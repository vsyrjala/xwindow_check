CPPFLAGS:=-Wall -Wextra
CFLAGS:=-O2 -g $(shell pkg-config --cflags pixman-1)
LDLIBS:=-lreadline -lX11 -lXcomposite $(shell pkg-config --libs pixman-1)

.PHONY: all clean

all: xwindow_check

xwindow_check: region.o xwindow_check.o

clean::
	-rm -rf xwindow_check *.o
